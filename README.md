# wpa

Wrap wpa\_supplicant to manage confs a little easier. Theoretically you can put your connections all into one conf but I like being able to actually explicitly connect to specific SSIDs and that just sets priorities and connects to the first one it can which isn't always what I want.

## Installation

```bash
git clone https://git.envs.net/lel/wpa
cd wpa
ln -s $(pwd)/wpa ~/.local/bin
```

## Usage

### But first!

At some point I'll add the ability to generate or add new configs but at the moment just drop a wpa\_supplicant config in ~/.wpa with a meaningful name ending in .conf. There are lots of places on the internet that show you how to make these and if you're at all interested in this script then you probably already know what you're doing on that front.


### Actual usage:

```bash
wpa [options] name
```

where `name` is the name of the .conf file dropped into .wpa (without the .conf), e.g. for 'tether.conf' it would be 'tether'.

To connect to an ssid:

```bash
wpa tether
```

-d disconnects from all connections.

```bash
wpa -d
```

## Misc. notes

This is really bad and I know that but I don't care. I figured maybe someone would find it useful so here it is. I haven't tested this outside my own setup at all and it's entirely possible that there will be little problems that will require you to edit it slightly. idk.
