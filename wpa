#!/usr/bin/env sh

OPTIND=1
disconnect=0
list=0
interface=wlp2s0
# set dhc_option to 0 for dhcpcd, 1 for dhclient; i feel like dhcpcd is neater for this script's purposes tho
dhc_option=0

if [ -z $WPA_DIR ]; then
  DIR="$HOME/.wpa"
else
  DIR="${WPA_DIR}"
fi

show_help() {
    echo "Usage: $(printf '%s\n' "${0##*/}") [options] device_name
    Connect to a wifi access point.

options:
  -h           display this help and exit
  -d           disconnect from ssids
  -l           list possible confs
  "
}

while getopts "hdl" opt; do
    case "$opt" in
    h)
        show_help
        exit 0
        ;;
    d)  disconnect=1
	;;
    l)  list=1
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [ "$disconnect" -eq "1" ]; then
  sudo kill -9 $(cat /var/run/wpa.pid 2>/dev/null) 2>/dev/null || sudo pkill -9 wpa_supplicant 2>/dev/null
  sudo ip addr flush dev $interface
  exit 0
fi

if [ "$list" -eq "1" ]; then
  ls $DIR | sed 's/\(.*\)\.conf/\1 /'
  exit 0
fi

if [ ! -f $DIR ]; then
  mkdir -p $DIR
fi

if [ -z "$1" ];
then
    echo Error: Insufficient arguments\; try $(printf '%s\n' "${0##*/}") -h >&2
    exit 1
fi

# haven't added ability to add/gen confs yet
# if [ -n "$new" ]; then
#   echo Adding $new to $DIR/$1.conf
#   echo $new > $DIR/$1.conf
#   exit 0
# fi

file=$DIR/$1.conf

if [ ! -f $file ]; then
  echo "Error: File '$file' not found." >&2
  exit 1
fi

# haven't added ability to remove confs yet b/c haven't added ability to add/gen them, so it didn't seem important
# if [ -n "$remove" ]; then
#   rm $file
#  exit 0
# fi

echo Dropping any previous connections...
sudo killall wpa_supplicant
if [ "$list" -eq "1" ]; then
  echo Flushing interface $interface...
  sudo ip addr flush dev $interface
fi

echo Initiating wpa_supplicant...
sudo wpa_supplicant -B -i $interface -c $file

echo Acquiring dhc lease...
if [ "$dhc_option" -eq "1" ]; then
  sudo dhclient $interface
else
  sudo dhcpcd -n
fi
